package com.legion.creatures;

/**
 * Родительский класс для сущностей.
 * Имеет все поля и методы для ФИО.
 */
public class Person {
    private String name;
    private String surname;
    private String patronymic;


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getFullName(){
         return surname+' '+name+' '+patronymic;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


}
